package com.nuance.clu.maven;


import java.util.List;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * Branch extension configuration Mojo.  This mojo is ONLY used to configure the extension.
 *
 * @author alan_yackel
 */
@Mojo( name = "branch" )
public class BranchMojo extends AbstractMojo
{

	@Parameter( defaultValue = "${project}", readonly = true )
    private MavenProject project;

    @Parameter( property = "branchProperty", required = false)
    private String branchProperty;

    /**
     * The root directory of the repository we want to check
     *
     */
    @Parameter( property = "gitDirectory", required = true )
    private String dotGitDirectory;

    @Parameter(defaultValue = "${plugin.artifacts}", readonly = true, property = "artifacts")
    private List artifacts;

    @Override
    public void execute()
        throws MojoExecutionException, MojoFailureException
    {
        getLog().info( "This mojo is used to configure an extension, and should NOT be executed directly." );
    }
}
