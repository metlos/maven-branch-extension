package com.nuance.clu.maven;

public interface GitFeatureInfo {

    String getFeatureRepositoryName();

    String getCurrentGitBranch();

    String getFeaturePrefix();
}