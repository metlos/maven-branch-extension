package com.nuance.clu.maven;

import java.io.File;
import java.io.IOException;

import org.apache.maven.MavenExecutionException;
import org.apache.maven.project.MavenProject;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

public class JGitProvider implements GitProvider {

    private final MavenProject mavenProject;
    private String gitDirectory;
    private Repository git;

    public JGitProvider(String gitDirectory, MavenProject mavenProject) {
        this.gitDirectory = gitDirectory;
        this.mavenProject = mavenProject;
    }

    @Override
    public void init() throws MavenExecutionException {
        git = getGitRepository();
    }

    private Repository getGitRepository() throws MavenExecutionException {
        Repository repository;
        File gitDirectoryFile = lookupGitDirectory(gitDirectory);

        FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
        try {
            repository = repositoryBuilder.setGitDir(gitDirectoryFile).readEnvironment()
                    .findGitDir().build();
        }
        catch (IOException e) {
            throw new MavenExecutionException("Could not initialize repository.", e);
        }

        if (repository == null) {
            throw new MavenExecutionException("Could not create git repository. Are you sure '"
                    + gitDirectory + "' is the valid Git root for your project?", new Throwable());
        }

        return repository;
    }

    public File lookupGitDirectory(String manuallyConfiguredDirString) {
        File manuallyConfiguredDir = new File(manuallyConfiguredDirString);

        if (manuallyConfiguredDir.exists()) {
            // If manuallyConfiguredDir is a directory then we can use it as the git path.
            if (manuallyConfiguredDir.isDirectory()) {
                return manuallyConfiguredDir;
            }
        }

        return findProjectGitDirectory();
    }

    private File findProjectGitDirectory() {
        if (mavenProject == null) {
            return null;
        }

        File basedir = mavenProject.getBasedir();
        while (basedir != null) {
            File gitdir = new File(basedir, Constants.DOT_GIT);
            if (gitdir != null && gitdir.exists()) {
                if (gitdir.isDirectory()) {
                    return gitdir;
                }
                else {
                    return null;
                }
            }
            basedir = basedir.getParentFile();
        }
        return null;
    }

    @Override
    public String getBranchName() throws IOException {
        String branch = git.getBranch();
        return branch;
    }

}
