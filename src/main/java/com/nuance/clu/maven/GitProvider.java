package com.nuance.clu.maven;

import java.io.IOException;

import org.apache.maven.MavenExecutionException;

public interface GitProvider {

	public abstract String getBranchName() throws IOException;

	public abstract void init() throws MavenExecutionException;

}